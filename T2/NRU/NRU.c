#include "NRU.h"

typedef enum esn
{

    eSN_Nao = 0,

    eSN_Sim = 1

}eSN;

typedef struct paginaNRU
{

    /* endereco logico relacionado ao processo */
    int endLogico;

    /*  Verificador de pagina Modificada */
    eSN M ;

    /* Verificador de pagina Referenciada */
    eSN R ;

    /* ultimo tempo de iteracao que houve referencia ao processo*/
    int ultimaReferencia ;


}PaginaNRU;

typedef struct memVirtualNRU
{

    /* Tamanho maximo de paginas */
    int tamMaxPaginas ;

    /* Deslocamento de uma pagina */
    int desloc ; 

    /* tempo Maxima que uma pagina pode estar como referenciada */
    int tempoMax;

    /* Indicador do proximo indice a ser preenchido */
    int proxIndice ;

    /* numero de Page faults e Sobre Escricoes */
    int pageFaults ;
    int paginasSujas ;

    /* Lista de Paginas da memória Virtual */
    PaginaNRU *paginas;


}MemVirtualNRU;

/* Métodos privados */

/* Define potencia do deslocamento */
static int DefineDeslocamento(int tamPagina);

/* Define qtd de paginas total suportado pela memoria */
static int DefineTamPaginas(int tamPagina, int tamMemVirtual);

/* Define exp relativo a Memoria fisica dada */
static int DefineExpFisica(int tamMemFisica);

/* Inicializa Paginas */
static PaginaNRU* CriaPaginas(int max) ;

/* Retorna indice do endLogico ou -1 caso nao exista o endLogico na memoria virtual */
static int EncontraPagina(MemVirtualNRU* memVirtual, int endLogico);

/* Retorna o indice de uma pagina a ser substituida  */
static int PaginaParaSubstituir(MemVirtualNRU* memVirtual);

/* Verifica e modifica todas as paginas na memoria caso necessário */
static void AtualizaMemVirtual(MemVirtualNRU* memVirtual, int tempoAtual);

/* Métodos exportados */

MemVirtualNRU* InicializaMemVirtual (int tamPagina, int tamMemFisica)
{
    MemVirtualNRU* new;

    new = (MemVirtualNRU*)malloc(sizeof(MemVirtualNRU));
    if (!new)
    {
        perror("Falha ao alocar memVirtual - InicializaMemVirtual");
        exit(1);
    }

    new->desloc = DefineDeslocamento(tamPagina);

    new->tamMaxPaginas = DefineTamPaginas(tamMemFisica, new->desloc);

    /* o tempo maximo é qnts iteracoes podem ocorrer ate que a pagina seja antiga */
    new->tempoMax = (int)(new->tamMaxPaginas / 4); 

    new->proxIndice = 0;

    new->pageFaults = 0;

    new->paginasSujas = 0;

    new->paginas = CriaPaginas(new->tamMaxPaginas);

    return new;

}

int DefineEndLogico(MemVirtualNRU* memVirtual, unsigned int addr)
{
    return (int) (addr >> memVirtual->desloc);
}

void LiberaMemVirtual(MemVirtualNRU* memVirtual)
{

    /* Libera paginas */
    free(memVirtual->paginas);

    /* Libera memoria */
    free(memVirtual);

    return;

}

int InserePagina(MemVirtualNRU* memVirtual, int page, char rw, int tempo)
{
    /* pagina a ser editada ou adicionada */
    PaginaNRU* editPagina;

    /* procura se existe o end logico na memoria  */
    int encontrouPag = EncontraPagina(memVirtual , page); 

    /* se existe lugar vazio */
    if (memVirtual->proxIndice >= 0)
    {
        editPagina = memVirtual->paginas + memVirtual->proxIndice;

        /* se chegou ao final, anula proxIndice */
        if (memVirtual->proxIndice + 1 == memVirtual->tamMaxPaginas)
        {
            memVirtual->proxIndice = -1;
        }
        else
        {
            memVirtual->proxIndice += 1;            
        }
        
    }
    else /* se nao tem lugar vazio */
    {
        /* se nao encontrou end logico na memoria */
        if (encontrouPag < 0)
        {
            memVirtual->pageFaults += 1;
            encontrouPag = PaginaParaSubstituir(memVirtual); 

            editPagina = memVirtual->paginas + encontrouPag;
            /* se a pagina foi modificada, ela é suja */
            if (editPagina->M )
            {
                memVirtual->paginasSujas += 1;
                editPagina->M = eSN_Nao;
            }
        }

        editPagina = memVirtual->paginas + encontrouPag;
    }

    /* atualiza pagina na memoria virtual */
    editPagina->endLogico = page ; 
    editPagina->R = eSN_Sim;
    editPagina->ultimaReferencia = tempo;
    if (rw == 'W')
    {
        editPagina->M = eSN_Sim;
    }

    AtualizaMemVirtual(memVirtual, tempo); 

}

int RetornaPageFaults(MemVirtualNRU* memVirtual)
{
    return memVirtual->pageFaults ;
}

int RetornaPaginasSujas(MemVirtualNRU* memVirtual)
{
    return memVirtual->paginasSujas ;
}

/* Métodos Privados  */

static int DefineExpFisica(int tamMemFisica)
{
    switch (tamMemFisica)
    {
    case 1:
        return 20;

    case 2:
        return 21;

    case 4:
        return 22;

    case 8:
        return 23;

    case 16:
        return 24;

    default:
        return 0;

    }
}

static int DefineDeslocamento(int tamPagina)
{
    switch (tamPagina)
    {
    case 8:
        return 13;

    case 16:
        return 14;
        
    case 32:
        return 15;
    
    default:
        return 0 ;
    }
}

static int DefineTamPaginas(int tamMemFisica, int desloc)
{
    int expFisica;
    int cont = 0 ;
    int total = 1 ;

    expFisica = DefineExpFisica(tamMemFisica); 

    if (!expFisica)
    {
        perror("Erro ao definir expFisica - DefineTamPaginas");
        exit(1);
    }
    int expoente = expFisica - desloc; 
    while (cont++ < expoente) total*=2;

    return total  ;

}

static PaginaNRU* CriaPaginas(int max) 
{
    PaginaNRU* new;
    int cont ;

    /* Aloca espaco para todas as paginas possiveis */
    new = (PaginaNRU*)malloc(sizeof(PaginaNRU) * max);
    if (!new)
    {
        perror("Erro ao alocar paginas");
        exit(1);
    }

    /* Inicializa paginas */
    for (cont = 0 ; cont < max ; cont++)
    {
        (new + cont)->endLogico = -1;
        new[cont].M = eSN_Nao ;
        new[cont].R = eSN_Nao ; 
        new[cont].ultimaReferencia = 0 ;
    }

    return new;
}

static int EncontraPagina(MemVirtualNRU* memVirtual, int endLogico)
{
    /* var de retorno */
    int indice = -1;

    /* var contadora */
    int cont = 0;
    PaginaNRU* pagina = memVirtual->paginas; 

    /* enquanto nao chega ao tamanho maximo  */
    while(cont < memVirtual->tamMaxPaginas)
    {
        if ((pagina + cont)->endLogico == endLogico)
        {
            indice = cont ;
            break;
        }
        cont++;
    }

    return indice ; 
}

static int PaginaParaSubstituir(MemVirtualNRU* memVirtual)
{
    int cont = 0 ;
    PaginaNRU* pagina;

    while((cont < memVirtual->tamMaxPaginas))
    {
        pagina = memVirtual->paginas + cont;

        if (!pagina->R)
        {
            break;
        }
        cont++;
    }

    return cont ;

}

static void AtualizaMemVirtual(MemVirtualNRU* memVirtual, int tempoAtual)
{
    int cont = 0 ;
    PaginaNRU* pagina = memVirtual->paginas;

    while((cont < memVirtual->tamMaxPaginas) && (pagina + cont)->endLogico != -1)
    {
        int tempoTotal = tempoAtual - (pagina + cont)->ultimaReferencia;
        if (tempoTotal == memVirtual->tempoMax)
        {
            (pagina+cont)->R = 0;
        }
        cont++;
    }

    return ;
}


