#include <stdio.h>
#include <stdlib.h>

/* Tipos da Estrutura */

typedef struct memVirtualNRU MemVirtualNRU;

/* Funções Exportadas pelo módulo */

/* Realiza a Inicializacao do simulador de memoria virtual */
MemVirtualNRU* InicializaMemVirtual (int tamPagina, int tamMemVirtual);

/* Insere um processo no algoritimo de memoria virtual */
int InserePagina(MemVirtualNRU* memVirtual, int page, char rw, int tempo);

/* Retorna o Endereco logico da pagina */
int DefineEndLogico (MemVirtualNRU* memVirtual, unsigned int addr);

/* Libera o espaco de memoria alocado pela memoria virtual */
void LiberaMemVirtual(MemVirtualNRU* memVirtual);

/* retorna o numero de PageFaults de uma memoria virtual */
int RetornaPageFaults(MemVirtualNRU* memVirtual);

/* retorna o numero de Sobreescricoes de uma memoria virtual */
int RetornaPaginasSujas(MemVirtualNRU* memVirtual);