#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "NRU/NRU.h"
#include "LFU/LFU.h"
#include "FIFO/FIFO.h"

/* Funções encapsuladas na Main */

static void RealizaAlgoritimoFIFO(char *arquivo, int tamPagina, int tamMemFisica);

static void RealizaAlgoritimoLFU(char *arquivo, int tamPagina, int tamMemFisica);

static void RealizaAlgoritimoNRU(char *arquivo, int tamPagina, int tamMemFisica);

/* ************************************************ */

int main(char argc, char *argv[])
{
    /* Argumentos passados */
    char algoritimo[10], arquivo[20];
    int tPagina, tMemFisica;

    if (argc != 5)
    {
        printf("Uso:\nsim-virtual <ALGORITMO> <arquivo> <tamanho_pagina> <tamanho_memoria>\n");
        return 0;
    }

    /* Coleta dos argumentos */
    sprintf(arquivo, "%s", argv[1]);
    sprintf(algoritimo, "%s", argv[2]);
    tPagina = atoi(argv[3]);
    tMemFisica = atoi(argv[4]);

    /* If's identificadores do algoritimo escolhido */
    if (!strcmp(algoritimo, "NRU"))
    {
        RealizaAlgoritimoNRU(arquivo, tPagina, tMemFisica);
    }
    else if (!strcmp(algoritimo, "LFU"))
    {

        RealizaAlgoritimoLFU(arquivo, tPagina, tMemFisica);
    }
    else if (!strcmp(algoritimo, "FIFO"))
    {

        RealizaAlgoritimoFIFO(arquivo, tPagina, tMemFisica);
    }

    return 0;
}

static void RealizaAlgoritimoNRU(char *arquivo, int tamPagina, int tamMemFisica)
{
    /* Valor do endereco do processo */
    unsigned int addr;

    /* tempo de iteracao */
    int tempo;

    /* referente a leitura ou escrita do processo */
    char rw;

    /* arquivo a ser lido */
    FILE *file;

    /* pagina,  deslocamento*/
    int page, desloc;

    /* Memória virtual */
    MemVirtualNRU *memVirtual;

    /* Abre file arquivo */
    file = fopen(arquivo, "r");
    if (!file)
    {
        perror("Erro ao abrir o arquivo - fopen");
        exit(1);
    }

    /* Funcao que armazena espaço para o Simulador e inicializa-o  */
    memVirtual = InicializaMemVirtual(tamPagina, tamMemFisica); // TODO
    if (!memVirtual)
    {
        perror("Erro ao inicializar a memoria virtual - InicializaMemVirtual(main)");
        exit(1);
    }

    /* define o tempo de iteracao inicial */
    tempo = 0;

    while (fscanf(file, "%x %c", &addr, &rw) != EOF)
    {
        /* Define Endereco Logico */
        page = DefineEndLogico(memVirtual, addr);

        if (!InserePagina(memVirtual, page, rw, tempo))
        {
            perror("Erro ao inserir processo - InserePagina(main)");
            printf("Arquivo : %s  \t\t Linha : %d\n", arquivo, tempo + 1);
            printf("addr : %x \t\t rw : %c\n", addr, rw);
            exit(1);
        }

        /* aumenta tempo */
        tempo++;
    }

    /* Saída dos resultados */
    puts("\nInicio do Quadro de resposta\n");
    printf("Arquivo de entrada          : %s    \n", arquivo);
    printf("Tamanho da memoria fisica   : %d MB \n", tamMemFisica);
    printf("Tamanho de pagina           : %d KB \n", tamPagina);
    printf("Algoritimo de Substituicao  : NRU   \n");
    printf("Número de Faltas de Paginas : %d    \n", RetornaPageFaults(memVirtual));
    printf("Número de Paginas escritas  : %d    \n", RetornaPaginasSujas(memVirtual));
    puts("Fim do Quadro de resposta\n");

    LiberaMemVirtual(memVirtual);

    fclose(file);

    return;
}

static void RealizaAlgoritimoFIFO(char *arquivo, int tamPagina, int tamMemFisica)
{
    /* Valor do endereco do processo */
    unsigned int addr ;

    /* tempo de iteracao */
    int tempo;

    /* referente a leitura ou escrita do processo */
    char rw ;

    /* arquivo a ser lido */
    FILE *file;

    /* pagina,  deslocamento*/
    int page, desloc;

    /* Memória virtual */
    MemVirtualFIFO *memVirtual ;
    
    /* Abre file arquivo */
    file = fopen(arquivo,"r");
    if (!file)
    {
        perror("Erro ao abrir o arquivo - fopen");
        exit(1);
    }

    /* Funcao que armazena espaço para o Simulador e inicializa-o  */
    memVirtual = InicializaMemVirtualFIFO(tamPagina, tamMemFisica);
    if (!memVirtual)
    {
        perror("Erro ao inicializar a memoria virtual - InicializaMemVirtual(main)");
        exit(1);
    }


    /* define o tempo de iteracao inicial */
    tempo = 0;

    while (fscanf(file, "%x %c", &addr, &rw) != EOF )
    {
        /* Define Endereco Logico */
        page = DefineEndLogicoFIFO(memVirtual, addr) ; 
        if (!InserePaginaFIFO(memVirtual , page, rw, tempo))
        {
            perror("Erro ao inserir processo - InserePaginaFIFO(main)");
            printf("Arquivo : %s  \t\t Linha : %d\n", arquivo, tempo + 1);
            printf("addr : %x \t\t rw : %c\n", addr, rw);
            exit(1);
        }

        /* aumenta tempo */
        tempo++;
    }
    
    /* Saída dos resultados */
    puts("\nInicio das respostas\n");
    printf("Arquivo de entrada          : %s    \n", arquivo);
    printf("Tamanho da memoria fisica   : %d MB \n", tamMemFisica);
    printf("Tamanho de pagina           : %d KB \n", tamPagina);
    printf("Algoritimo de Substituicao  : FIFO   \n");
    printf("Número de Faltas de Paginas : %d    \n", RetornaPageFaultsFIFO(memVirtual));
    printf("Número de Paginas escritas  : %d    \n", RetornaPaginasSujasFIFO(memVirtual));
    printf("Número de Primeiras Chances : %d    \n", RetornaPrimeirasChancesFIFO(memVirtual));
    printf("Número de Segundas Chances  : %d    \n", RetornaSegundasChancesFIFO(memVirtual));
    puts("Fim da Execucao\n");

    return ;
}

static void RealizaAlgoritimoLFU(char *arquivo, int tamPagina, int tamMemFisica)
{
    lfu_setup(tamPagina, tamMemFisica);

    FILE *file = fopen(arquivo, "r");

    unsigned int addr;
    char rw;
    int count = 0;

    while (fscanf(file, "%x %c\n", &addr, &rw) == 2)
    {
        lfu_access(addr, rw);
    }

    lfu_finish();

    fclose(file);

    return;
}