#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include "FIFO.h"

Fila * criaFila()
{
	Fila *nova = (Fila*)malloc(sizeof(Fila));

	nova->head = NULL;
	nova->tail = NULL;

	return nova;
}

Fila* ehVazia(Fila* fila)
{
	if ((fila->head) == NULL) return 1;

	return 0;
}

void insere(Fila* fila, int elemento)
{
	Node *novo = (Node*)malloc(sizeof(Node));

	novo->elemento = elemento;	
	
	if (ehVazia(fila))
	{
		novo->prox = NULL;
		fila->head = novo;
		fila->tail = novo;
	}
	else
	{
		novo->prox = fila->tail;
		fila->tail = novo;
	}
}

void printa(Fila *fila)
{
	Node *aux;
	aux = fila->tail;

	while (aux != NULL)
	{
		printf("%d\n", aux->elemento);
		aux = aux->prox;
	}
}

void removeFila(Fila *fila)
{
	Node *aux = fila->tail;
	Node *previous_node;

	if (ehVazia(fila))
	{
		printf("Fila vazia\n");
		exit(1);
	}
	else
	{
		while (aux != NULL)
		{
			if (aux->prox!=NULL)
			{
				previous_node = aux;
			}
			aux = aux->prox;
		}
		previous_node->prox = NULL;
		fila->head = previous_node;
	}
}

int main(void)
{
	Fila *nova = criaFila();
}

