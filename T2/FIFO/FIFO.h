#include <stdio.h>
#include <stdlib.h>

/* Tipos da Estrutura */

struct node {
	struct node *prox;
    int chance;
	struct paginaFIFO * pagina;

}; typedef struct node Node;

struct fila {
	Node *head;
	Node *tail;
    int quantidade;

}; typedef struct fila Fila;

typedef enum esn
{

    eSN_Nao = 0,

    eSN_Sim = 1

}eSN;

typedef struct paginaFIFO
{

    /* endereco logico relacionado ao processo */
    int endLogico;

    /*  Verificador de pagina Modificada */
    eSN M ;

    /* Verificador de pagina Referenciada */
    eSN R ;

    /* ultimo tempo de iteracao que houve referencia ao processo*/
    int ultimaReferencia ;


} PaginaFIFO;

typedef struct memVirtualFIFO
{

    /* Tamanho maximo de paginas */
    int tamMaxPaginas ;

    /* Deslocamento de uma pagina */
    int desloc ; 

    /* tempo Maxima que uma pagina pode estar como referenciada */
    int tempoMax;

    /* numero de Page faults e Sobre Escricoes */
    int pageFaults ;
    int paginasSujas ;
	int primeirasChances ;
	int segundasChances;

    /* Lista de Paginas da memória Virtual */
    PaginaFIFO *paginas;

	/*fifo second chance*/
	Fila * fila;


}MemVirtualFIFO;


/* Funções Exportadas pelo módulo */

/* Realiza a Inicializacao do simulador de memoria virtual */
MemVirtualFIFO* InicializaMemVirtualFIFO (int tamPagina, int tamMemVirtual);

/* Insere um processo no algoritimo de memoria virtual */
int InserePaginaFIFO(MemVirtualFIFO* memVirtual, int page, char rw, int tempo);

/* Retorna o Endereco logico da pagina */
int DefineEndLogicoFIFO (MemVirtualFIFO* memVirtual, unsigned int addr);

/* Libera o espaco de memoria alocado pela memoria virtual */
void LiberaMemVirtualFIFO(MemVirtualFIFO* memVirtual);

/* retorna o numero de PageFaults de uma memoria virtual */
int RetornaPageFaultsFIFO(MemVirtualFIFO* memVirtual);

/* retorna o numero de Sobreescricoes de uma memoria virtual */
int RetornaPaginasSujasFIFO(MemVirtualFIFO* memVirtual);

/* retorna o numero de Sobreescricoes de uma memoria virtual */
int RetornaPrimeirasChancesFIFO(MemVirtualFIFO* memVirtual);

/* retorna o numero de Sobreescricoes de uma memoria virtual */
int RetornaSegundasChancesFIFO(MemVirtualFIFO* memVirtual);


Fila* criaFila();
int ehVazia(Fila* fila);
void insere(Fila* fila, PaginaFIFO * elemento);
void insereSemChance(Fila* fila, PaginaFIFO * elemento);
void printa(Fila *fila);
Node * removeFila(Fila *fila);

