#include "FIFO.h"

/* Métodos privados */

/* Define potencia do deslocamento */
static int DefineDeslocamento(int tamPagina); 

/* Define qtd de paginas total suportado pela memoria */
static int DefineTamPaginas(int tamPagina, int tamMemVirtual);

/* Define exp relativo a Memoria fisica dada */
static int DefineExpFisica(int tamMemFisica);

/* Inicializa Paginas */
static PaginaFIFO* CriaPaginas(int max) ;

/* Retorna indice do endLogico ou -1 caso nao exista o endLogico na memoria virtual */
static int EncontraPagina(MemVirtualFIFO* memVirtual, int endLogico);

/* Retorna o indice de uma pagina a ser substituida  */
static int PaginaParaSubstituir(MemVirtualFIFO* memVirtual);

/* Verifica e modifica todas as paginas na memoria caso necessário */
static void AtualizaMemVirtual(MemVirtualFIFO* memVirtual, int tempoAtual);

/* Métodos exportados */

MemVirtualFIFO * InicializaMemVirtualFIFO (int tamPagina, int tamMemFisica)
{

	Fila * fila = criaFila();
    MemVirtualFIFO * new;

    new = (MemVirtualFIFO *)malloc(sizeof(MemVirtualFIFO));
    if (!new)
    {
        perror("Falha ao alocar memVirtual - InicializaMemVirtual");
        exit(1);
    }

    new->desloc = DefineDeslocamento(tamPagina);

    new->tamMaxPaginas = DefineTamPaginas(tamMemFisica, new->desloc);

    /* o tempo maximo é qnts iteracoes podem ocorrer ate que a pagina seja antiga */
    new->tempoMax = (int)(new->tamMaxPaginas / 4); 

    new->pageFaults = 0;

    new->paginasSujas = 0;

    new->paginas = CriaPaginas(new->tamMaxPaginas);

    new->fila = fila;

    return new;

}

int DefineEndLogicoFIFO(MemVirtualFIFO* memVirtual, unsigned int addr)
{
    return (int) (addr >> memVirtual->desloc);
}

void LiberaMemVirtualFIFO(MemVirtualFIFO* memVirtual)
{
	free(memVirtual->fila);

    /* Libera paginas */
    free(memVirtual->paginas);


    /* Libera memoria */
    free(memVirtual);

    return;

}

int InserePaginaFIFO(MemVirtualFIFO* memVirtual, int page, char rw, int tempo)
{
    /* pagina a ser editada ou adicionada */
    PaginaFIFO * editPagina;

    /* procura se existe o end logico na memoria  */
    int encontrouPag = EncontraPagina(memVirtual , page); 
    if (memVirtual->fila->quantidade != memVirtual->tamMaxPaginas)
    {
        editPagina = memVirtual->paginas + memVirtual->fila->quantidade;
		insere(memVirtual->fila, editPagina); 
		memVirtual->fila->quantidade++;
    }
    else /* se nao tem lugar vazio */
    {
		
        /* se nao encontrou end logico na memoria */
        if (encontrouPag < 0)
        { 
			//Segunda chance
			if(memVirtual->fila->head->chance == 1)
			{
				memVirtual->primeirasChances++;
				Node * no = removeFila(memVirtual->fila); //Tiro do topo e boto no tail com 0 chances
				insereSemChance(memVirtual->fila, no->pagina);
			}
			//Sem mais chances
			else if(memVirtual->fila->head->chance == 0)
			{
				memVirtual->segundasChances+=1;
				removeFila(memVirtual->fila);	
				memVirtual->fila->quantidade-=1;		        
				memVirtual->pageFaults += 1;
		        encontrouPag = PaginaParaSubstituir(memVirtual); 
				
				editPagina = memVirtual->paginas + encontrouPag;
		        /* se a pagina foi modificada, ela é suja */
		        if (editPagina->M )
		        {
		            memVirtual->paginasSujas += 1;
		            editPagina->M = eSN_Nao;
		        }
				/* atualiza pagina na memoria virtual */
				editPagina->endLogico = page ; 
				editPagina->R = eSN_Sim;
				editPagina->ultimaReferencia = tempo;
				if (rw == 'W')
				{
					editPagina->M = eSN_Sim;
				}

				AtualizaMemVirtual(memVirtual, tempo); 
			}
        }
    }

}

int RetornaPageFaultsFIFO(MemVirtualFIFO * memVirtual)
{
    return memVirtual->pageFaults ;
}

int RetornaPaginasSujasFIFO(MemVirtualFIFO * memVirtual)
{
    return memVirtual->paginasSujas ;
}

int RetornaPrimeirasChancesFIFO(MemVirtualFIFO * memVirtual)
{
    return memVirtual->primeirasChances ;
}

int RetornaSegundasChancesFIFO(MemVirtualFIFO * memVirtual)
{
    return memVirtual->segundasChances ;
}

/* Métodos Privados  */

static int DefineExpFisica(int tamMemFisica)
{
    switch (tamMemFisica)
    {
    case 1:
        return 20;

    case 2:
        return 21;

    case 4:
        return 22;

    case 8:
        return 23;

    case 16:
        return 24;

    default:
        return 0;

    }
}

static int DefineDeslocamento(int tamPagina)
{
    switch (tamPagina)
    {
    case 8:
        return 13;

    case 16:
        return 14;
        
    case 32:
        return 15;
    
    default:
        return 0 ;
    }
}

static int DefineTamPaginas(int tamMemFisica, int desloc)
{
    int expFisica;
    int cont = 0 ;
    int total = 1 ;

    expFisica = DefineExpFisica(tamMemFisica); 

    if (!expFisica)
    {
        perror("Erro ao definir expFisica - DefineTamPaginas");
        exit(1);
    }
    int expoente = expFisica - desloc; 
    while (cont++ < expoente) total*=2;

    return total  ;

}

static PaginaFIFO * CriaPaginas(int max) 
{
    PaginaFIFO * new;
    int cont ;

    /* Aloca espaco para todas as paginas possiveis */
    new = (PaginaFIFO*)malloc(sizeof(PaginaFIFO) * max);
    if (!new)
    {
        perror("Erro ao alocar paginas");
        exit(1);
    }

    /* Inicializa paginas */
    for (cont = 0 ; cont < max ; cont++)
    {
        (new + cont)->endLogico = -1;
        new[cont].M = eSN_Nao ;
        new[cont].R = eSN_Nao ; 
        new[cont].ultimaReferencia = 0 ;
    }

    return new;
}

static int EncontraPagina(MemVirtualFIFO * memVirtual, int endLogico)
{
    /* var de retorno */
    int indice = -1;

    /* var contadora */
    int cont = 0;
    PaginaFIFO * pagina = memVirtual->paginas; 

    /* enquanto nao chega ao tamanho maximo  */
    while(cont < memVirtual->tamMaxPaginas)
    {
        if ((pagina + cont)->endLogico == endLogico)
        {
            indice = cont ;
            break;
        }
        cont++;
    }

    return indice ; 
}

static int PaginaParaSubstituir(MemVirtualFIFO * memVirtual)
{
    int cont = 0 ;
    PaginaFIFO * pagina;

    while((cont < memVirtual->tamMaxPaginas))
    {
        pagina = memVirtual->paginas + cont;

        if (!pagina->R)
        {
            break;
        }
        cont++;
    }

    return cont ;

}

static void AtualizaMemVirtual(MemVirtualFIFO * memVirtual, int tempoAtual)
{
    int cont = 0 ;
    PaginaFIFO * pagina = memVirtual->paginas;

    while((cont < memVirtual->tamMaxPaginas) && (pagina + cont)->endLogico != -1)
    {
        int tempoTotal = tempoAtual - (pagina + cont)->ultimaReferencia;
        if (tempoTotal == memVirtual->tempoMax)
        {
            (pagina+cont)->R = 0;
        }
        cont++;
    }

    return ;
}

Fila * criaFila()
{
	Fila *nova = (Fila*)malloc(sizeof(Fila));
	nova->head = NULL;
	nova->tail = NULL;
	nova->quantidade = 0;

	return nova;
}

int ehVazia(Fila* fila)
{
	if ((fila->head) == NULL) return 1;

	return 0;
}

void insere(Fila* fila, PaginaFIFO * elemento)
{
	Node *novo = (Node*)malloc(sizeof(Node));

	novo->pagina = elemento;	
	novo->chance = 1;
	if (ehVazia(fila))
	{
		novo->prox = NULL;
		fila->head = novo;
		fila->tail = novo;
	}
	else
	{
		novo->prox = fila->tail;
		fila->tail = novo;
	}
}

void insereSemChance(Fila* fila, PaginaFIFO * elemento){
	Node *novo = (Node*)malloc(sizeof(Node));

	novo->pagina = elemento;	
	novo->chance = 0;
	if (ehVazia(fila))
	{
		novo->prox = NULL;
		fila->head = novo;
		fila->tail = novo;
	}
	else
	{
		novo->prox = fila->tail;
		fila->tail = novo;
	}
}

void printa(Fila *fila)
{
	Node *aux;
	aux = fila->tail;

	while (aux != NULL)
	{
		printf("End Logico: %d ultimaReferencia: %d\n", aux->pagina->endLogico, aux->pagina->ultimaReferencia);
		aux = aux->prox;
	}
}

Node * removeFila(Fila *fila)
{
	Node *aux = fila->tail;
	Node *previous_node;
	Node * removido;
	if (ehVazia(fila))
	{
		printf("Fila vazia\n");
		exit(1);
	}
	else
	{
		while (aux != NULL)
		{
			if (aux->prox!=NULL)
			{
				previous_node = aux;
			}
			aux = aux->prox;
		}
		removido = previous_node->prox;
		previous_node->prox = NULL;
		fila->head = previous_node;
	}
	return removido;
}


