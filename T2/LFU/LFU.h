void lfu_setup(int page_size, int mem_size);

void lfu_access(int addr, char rw);

void lfu_finish();
