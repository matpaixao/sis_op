#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <limits.h>

int page; //tamanho em KB
int mem; //tamanho em MB
int mem_pages; //numero de paginas que cabem na memoria
int table_entries;
int page_faults;
int overwritten;
int time;
int memory_full;

struct table_line{
	char M; //pagina modificada
	char R; //pagina referenciada
	int lastAccess;
	int uses;
	int realAddress;
};

struct table_line * table;

int * free_pages; //vetor que armazerna a informação se cada espaço na memoria esta livre

void lfu_setup(int page_size, int memory_size){
	page = page_size;
	mem = memory_size;
	mem_pages = memory_size * 1024 / page;
	table_entries = pow(2,22)/page;

	int count = 0;
	for(int i=0; i < table_entries; i++){
		count++;
	}
	printf("Tamanho de pagina: %dKB\n", page);
	printf("Memoria: %dMB\n", mem);

	table = (struct table_line*)malloc(table_entries * sizeof(struct table_line));
	for(int i = 0; i < table_entries; i++){
		table[i].realAddress = -1; //todas as paginas começam ausentes
	}

	free_pages = (int*)malloc(mem_pages * sizeof(int));
	for(int i = 0; i < mem_pages; i++)
		free_pages[i] = 1; //a memoria começa toda livre

	page_faults = 0;
	overwritten = 0;
	time = 0;
	memory_full = 0;
}

int expel_page(){
	int minPriority = INT_MAX;
	int priority;
	int target;
	for(int i = 0; i < table_entries; i++){
		if(table[i].realAddress == -1) continue;
		priority = 50 * table[i].uses - (time - table[i].lastAccess);
		if(priority < minPriority){
			target = i;
			minPriority = priority;
		}
	}
	if(table[target].M)
		overwritten++;
	int addr = table[target].realAddress;
	table[target].realAddress = -1;
	return addr;
}

void push_page(int page){
	int addr;
	if(!memory_full){
		addr = -1;
		for(int i = 0; i < mem_pages; i++){
			if(free_pages[i]){
				addr = i;
				free_pages[i] = 0;
				break;
			}
		}
		if(addr == -1){
			memory_full = 1;
			addr = expel_page();
		}
	}
	else{
		addr = expel_page();
	}
	table[page].realAddress = addr;
	table[page].M = 0;
	table[page].uses = 0;
}

void lfu_access(unsigned int addr, char rw){
	time++;
	int targetPage = (addr >> 10)/page;
	if(table[targetPage].realAddress == -1){ //page fault
		page_faults++;
		push_page(targetPage);
	}
	if(rw == 'W'){
		table[targetPage].M  = 1;
	}
	table[targetPage].R = 1;
	table[targetPage].lastAccess = time;
	table[targetPage].uses++;
}

void lfu_finish(){
	free(table);
	free(free_pages);
	printf("Número de Faltas de Páginas: %d\nNúmero de Páginas Escritas: %d\n\n", page_faults, overwritten);
}
